import styled from 'styled-components';
import { CenterColumnStyle } from '@make.org/ui/elements/FlexElements';

export const LoadMoreWrapperStyle = styled(CenterColumnStyle)`
  margin-top: 20px;
`;
