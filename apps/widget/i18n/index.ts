import german from '@make.org/widget/i18n/de.json';
import english from '@make.org/widget/i18n/en.json';
import french from '@make.org/widget/i18n/fr.json';

export const translationRessources = {
  en: { translation: english },
  fr: { translation: french },
  de: { translation: german },
};
