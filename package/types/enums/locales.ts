export enum LocaleType {
  fr = 'fr',
  en = 'en',
  de = 'de',
  uk = 'uk',
  cs = 'cs',
}
