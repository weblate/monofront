export const spacings = {
  xs: '5px',
  s: '10px',
  sm: '15px',
  m: '20px',
  l: '30px',
  xl: '50px',
  xxl: '100px',
};
