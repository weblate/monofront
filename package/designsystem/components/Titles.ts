import styled from 'styled-components';
import { typography } from '@make.org/designsystem/tokens/typography';

export const TitleXXXL = styled.h1`
  font-size: ${typography.FontSize.Universe};
  font-family: ${typography.FontFamily.Hightlight};
  font-weight: bold;
  line-height: ${typography.LineHeight.l140};
`;

export const TitleXXL = styled.h1`
  font-size: ${typography.FontSize.Earth};
  font-family: ${typography.FontFamily.Hightlight};
  font-weight: bold;
  line-height: ${typography.LineHeight.l140};
`;

export const TitleXL = styled.h2`
  font-size: ${typography.FontSize.Europe};
  font-family: ${typography.FontFamily.Hightlight};
  font-weight: bold;
  line-height: ${typography.LineHeight.l140};
`;

export const TitleL = styled.h2`
  font-size: ${typography.FontSize.France};
  font-family: ${typography.FontFamily.Hightlight};
  font-weight: bold;
  line-height: ${typography.LineHeight.l150};
`;

export const TitleM = styled.h2`
  font-size: ${typography.FontSize.IleDeFrance};
  font-family: ${typography.FontFamily.Hightlight};
  font-weight: bold;
  line-height: ${typography.LineHeight.l150};
`;

export const TitleS = styled.h4`
  font-size: ${typography.FontSize.GrandeCouronne};
  font-family: ${typography.FontFamily.Hightlight};
  font-weight: bold;
  line-height: ${typography.LineHeight.l150};
`;

export const TitleXS = styled.h1`
  font-size: ${typography.FontSize.PetiteCouronne};
  font-family: ${typography.FontFamily.Hightlight};
  font-weight: bold;
  line-height: ${typography.LineHeight.l150};
`;

export const TitleXXS = styled.h3`
  font-size: ${typography.FontSize.Paris};
  font-family: ${typography.FontFamily.Hightlight};
  font-weight: bold;
  line-height: ${typography.LineHeight.l150};
`;

export const TitleXXXS = styled.h1`
  font-size: ${typography.FontSize.Arrondissement};
  font-family: ${typography.FontFamily.Hightlight};
  font-weight: bold;
  line-height: ${typography.LineHeight.l150};
`;
