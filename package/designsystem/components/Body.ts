import styled from 'styled-components';
import { typography } from '@make.org/designsystem/tokens/typography';

export const BodyLDefault = styled.p`
  font-size: ${typography.FontSize.PetiteCouronne};
  font-family: ${typography.FontFamily.Default};
  line-height: ${typography.LineHeight.l150};
`;

export const BodyLHighLight = styled.p`
  font-size: ${typography.FontSize.PetiteCouronne};
  font-family: ${typography.FontFamily.Hightlight};
  font-weight: bold;
  line-height: ${typography.LineHeight.l150};
`;

export const BodyMDefault = styled.p`
  font-size: ${typography.FontSize.Arrondissement};
  font-family: ${typography.FontFamily.Default};
  line-height: ${typography.LineHeight.l150};
`;

export const BodyMHighLight = styled.p`
  font-size: ${typography.FontSize.Arrondissement};
  font-family: ${typography.FontFamily.Hightlight};
  font-weight: bold;
  line-height: ${typography.LineHeight.l150};
`;

export const BodyMCondensed = styled.p`
  font-size: ${typography.FontSize.Arrondissement};
  font-family: ${typography.FontFamily.Condensed};
  line-height: ${typography.LineHeight.l100};
`;

export const BodySDefault = styled.p`
  font-size: ${typography.FontSize.Bastille};
  font-family: ${typography.FontFamily.Default};
  line-height: ${typography.LineHeight.l150};
`;

export const BodySHighLight = styled.p`
  font-size: ${typography.FontSize.Bastille};
  font-family: ${typography.FontFamily.Hightlight};
  font-weight: bold;
  line-height: ${typography.LineHeight.l150};
`;

export const BodySCondensed = styled.p`
  font-size: ${typography.FontSize.Bastille};
  font-family: ${typography.FontFamily.Condensed};
  line-height: ${typography.LineHeight.l100};
`;

export const BodyXSDefault = styled.p`
  font-size: ${typography.FontSize.RueDeLappe};
  font-family: ${typography.FontFamily.Default};
  line-height: ${typography.LineHeight.l150};
`;

export const BodyXSHighlight = styled.p`
  font-size: ${typography.FontSize.RueDeLappe};
  font-family: ${typography.FontFamily.Hightlight};
  font-weight: bold;
  line-height: ${typography.LineHeight.l150};
`;

export const BodyXSCondensed = styled.p`
  font-size: ${typography.FontSize.RueDeLappe};
  font-family: ${typography.FontFamily.Condensed};
  line-height: ${typography.LineHeight.l100};
`;
